This addon is an external module for [pfUI](https://gitlab.com/shagu/pfUI) addon.

## Screenshots
![bottom](bottom.png)
![left](left.png)

## Description
Addon gathers minimap buttons of different addons, similar to [MBB](https://github.com/laytya/MinimapButtonBag-vanilla),  but with some love to [pfUI](https://gitlab.com/shagu/pfUI) :P

* Adds new frame for addon buttons
    * not movable
    * tied to minimap (`pfUI.minimap` frame)
    * can be placed to the left or at the bottom of minimap (`pfUI.minimap` frame)
    * fixed width/height depending on position
* Scans minimap (`Minimap` and `MinimapBackdrop` frames) for possible addon buttons and process them
    * backs up original button settings
    * scales and moves button to the new frame

## Installation
**This addon will not function without [pfUI](https://gitlab.com/shagu/pfUI) installed**
1. Download **[Latest Version](https://gitlab.com/dein0s_wow_vanilla/pfUI-addonbuttons/-/archive/master/pfUI-master.zip)**
2. Unpack the Zip file
3. Rename the folder to "pfUI-addonbuttons"
4. Copy "pfUI-addonbuttons" into Wow-Directory\Interface\AddOns
5. Restart WoW

## Commands
```
/abp add      - Manually process button under cursor
/abp del      - Manually remove buttun under cursor from the frame and restore original settings
/abp reset    - Reset lists of added and removed buttons
```
