-- load pfUI environment
setfenv(1, pfUI:GetEnvironment())

function pfUI:LoadConfig_addonbuttons()
  pfUI:UpdateConfig("addonbuttons", nil,         "position",    "bottom") 
  pfUI:UpdateConfig("addonbuttons", nil,         "rowsize",          "6") 
  pfUI:UpdateConfig("addonbuttons", nil,         "spacing",          "2") 
  pfUI:UpdateConfig("addonbuttons", nil,         "updateinterval",   "3") 
  pfUI:UpdateConfig("addonbuttons", nil,         "hideincombat",     "1") 
end
