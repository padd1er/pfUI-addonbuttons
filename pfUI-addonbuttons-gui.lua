-- load pfUI environment
setfenv(1, pfUI:GetEnvironment())

function pfUI:LoadGui_addonbuttons()
  if not pfUI.gui then return end

  local CreateConfig = pfUI.gui.CreateConfig
  local update = pfUI.gui.update
  
  pfUI.gui.dropdowns.addonbuttons_position = {
    "bottom:" .. T["Bottom"], 
    "left:" .. T["Left"], 
  }
  if pfUI.gui.tabs.thirdparty then
    pfUI.gui.tabs.thirdparty.tabs.addonbuttons = pfUI.gui.tabs.thirdparty.tabs:CreateTabChild(T["Addon Buttons"], true)
    pfUI.gui.tabs.thirdparty.tabs.addonbuttons:SetScript("OnShow", function() 
      if not this.setup then
        CreateConfig(update["addonbuttons"], this, T["Addon Buttons Panel Position"], C.addonbuttons, "position", "dropdown", pfUI.gui.dropdowns.addonbuttons_position) 
        CreateConfig(update["addonbuttons"], this, T["Number Of Buttons Per Row/Column"], C.addonbuttons, "rowsize")
        CreateConfig(update["addonbuttons"], this, T["Button Spacing"], C.addonbuttons, "spacing")
        CreateConfig(update["addonbuttons"], this, T["Hide When Entering Combat"], C.addonbuttons, "hideincombat", "checkbox")
        this.setup = true 
      end
    end)
  end
end
